/*
This file is the main player and window of the KUSF.org app for the Android OS.
Here a player is initiated and can be controlled with the play and stop button.
Along with player there is a control to output parse XML for the *most recent* song.
Updates will include a faster load time and a more recent parse of the playlist.
*/

package com.kusforg.android;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;


public class MusicAndroidActivity extends ActionBarActivity {

    static MediaPlayer mPlayer;
    Button buttonPlay;
    Button buttonStop;
    ProgressDialog progressBar;
    String url = "http://www.live365.com/play/294306";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(com.kusforg.android.R.menu.activity_main_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case com.kusforg.android.R.id.action_About:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            case com.kusforg.android.R.id.action_Playlist:
                startActivity(new Intent(this, playlistActivity.class));
                return true;
            case com.kusforg.android.R.id.action_mode_close_button:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.kusforg.android.R.layout.main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Drawable res = getResources().getDrawable(com.kusforg.android.R.drawable.ab_transparent_example);
        getSupportActionBar().setBackgroundDrawable(res);
        buttonPlay = (Button) findViewById(com.kusforg.android.R.id.play);
        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();
        //        public void schedule (TimerTask task, long delay, long period)
        //        Schedule a task for repeated fixed-delay execution after a specific delay.
        //
        //        Parameters
        //        task  the task to schedule.
        //        delay  amount of time in milliseconds before first execution.
        //        period  amount of time in milliseconds between subsequent executions.

        myTimer.schedule(myTask, 0, 180000);
        if (!isTaskRoot())
        {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }
        buttonPlay.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {

                    case com.kusforg.android.R.id.play:

                        new AsyncTask<Void, Void, Void>() {

                            @Override
                            protected void onPreExecute() {
                                progressBar = ProgressDialog.show(MusicAndroidActivity.this, "", "Loading. Please wait...", true);
                                mPlayer = new MediaPlayer();
                                try {
                                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    mPlayer.setDataSource(url);
                                } catch (IllegalArgumentException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IllegalStateException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                }
                            @Override
                            protected Void doInBackground(Void... params) {
                                // TODO Auto-generated method stub
                                //progressBar =ProgressDialog.show(MusicAndroidActivity.this, "", "Loading. Please wait...", true);
                                return null;
                            }
                            protected void onPostExecute(Void result) {
                               // progressBar =ProgressDialog.show(MusicAndroidActivity.this, "", "Loading. Please wait...", true);
                                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    public void onPrepared(MediaPlayer mp) {
                                        progressBar.dismiss();
                                        mPlayer.start();
                                    }
                                });
                                mPlayer.prepareAsync();
                                buttonPlay.setVisibility(Button.GONE);
                                buttonStop.setVisibility(Button.VISIBLE);

                            }
                        }.execute();
                }
            }

        });
        buttonStop = (Button) findViewById(com.kusforg.android.R.id.stop);
        buttonStop.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                switch (v.getId()) {

                    case com.kusforg.android.R.id.stop:
                        // TODO Auto-generated method stub
                        if (mPlayer != null && mPlayer.isPlaying())

                        {
                            mPlayer.stop();
                            buttonStop.setVisibility(Button.GONE);
                            buttonPlay.setVisibility(Button.VISIBLE);
                            break;
                        }
                }
            }
        });
        // hTextView = (TextView)findViewById(R.id.idTextView);


        Log.i("StackSites", "OnCreate()");

        //Get reference to our ListView
        sitesList = (ListView) findViewById(com.kusforg.android.R.id.sitesList);


		/*
		 * If network is available download the xml from the Internet.
		 * If not then try to use the local file from last time.
		 */
    }
    class MyTimerTask extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) {
                Log.i("StackSites", "starting download Task");
                SitesDownloadTask download = new SitesDownloadTask();
                download.execute();
            } else {
                mAdapter = new SitesAdapter(getApplicationContext(), -1, SitesXmlPullParser.getStackSitesFromFile(MusicAndroidActivity.this));
                sitesList.setAdapter(mAdapter);
            }
        }
    }
    protected void onDestroy() {
        super.onDestroy();
        // TODO Auto-generated method stub
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }
    private SitesAdapter mAdapter;
    private ListView sitesList;

    //Helper method to determine if Internet connection is available.
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * AsyncTask that will download the xml file for us and store it locally.
     * After the download is done we'll parse the local file.
     */

    public class SitesDownloadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            //Download the file
            try {
                Downloader.DownloadFromUrl("http://kusf.radioactivity.fm/feeds/last10.xml", openFileOutput("StackSites.xml", Context.MODE_PRIVATE));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //setup our Adapter and set it to the ListView.
            mAdapter = new SitesAdapter(MusicAndroidActivity.this, -1, SitesXmlPullParser.getStackSitesFromFile(MusicAndroidActivity.this));
            sitesList.setAdapter(mAdapter);
            Log.i("StackSites", "adapter size = " + mAdapter.getCount());
        }

    }
}

