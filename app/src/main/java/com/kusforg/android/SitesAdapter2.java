package com.kusforg.android;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Dean on 8/6/2015.
 */
public class SitesAdapter2  extends ArrayAdapter<StackSite> {

    public SitesAdapter2 (Context ctx, int textViewResourceId, List<StackSite> sites) {
        super(ctx, textViewResourceId, sites);
    }


	/*
	 * (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 *
	 * This method is responsible for creating row views out of a StackSite object that can be put
	 * into our ListView
	 */

    @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        TableLayout row = (TableLayout)convertView;
        Log.i("StackSites", "getView pos = " + pos);
        if(null == row){
            //No recycled View, we have to inflate one.
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = (TableLayout)inflater.inflate(com.kusforg.android.R.layout.row_site2, null);

        }


        //Get our View References
        TextView trackTxt = (TextView)row.findViewById(com.kusforg.android.R.id.trackTxt);
        TextView artistTxt = (TextView)row.findViewById(com.kusforg.android.R.id.artistTxt);
        TextView albumTxt = (TextView)row.findViewById(com.kusforg.android.R.id.albumTxt);
        TextView aboutTxt = (TextView)row.findViewById(com.kusforg.android.R.id.aboutTxt);

        //Set the relavent text in our TextViews
        trackTxt.setText("Track: "+(getItem(pos).getTrack()));
        artistTxt.setText("Artist: "+(getItem(pos).getArtist()));
        albumTxt.setText("Album: "+(getItem(pos).getAlbum()));
        aboutTxt.setText(getItem(pos).getAbout());

        return row;

    }

}
