package com.kusforg.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.util.Timer;
import java.util.TimerTask;

public class playlistActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.kusforg.android.R.layout.activity_playlist2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Drawable res = getResources().getDrawable(com.kusforg.android.R.drawable.ab_transparent_example);
        getSupportActionBar().setBackgroundDrawable(res);
        MyTimerTask myTask = new MyTimerTask();
        Timer myTimer = new Timer();

        myTimer.schedule(myTask, 0, 180000);
        Log.i("StackSites", "OnCreate()");

        //Get reference to our ListView
        sites2List = (ListView) findViewById(com.kusforg.android.R.id.sites2List);


		/*
		 * If network is available download the xml from the Internet.
		 * If not then try to use the local file from last time.
		 */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(com.kusforg.android.R.menu.menu_playlist, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case com.kusforg.android.R.id.action_About:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            case com.kusforg.android.R.id.action_mode_close_button:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            if (isNetworkAvailable()) {
                Log.i("StackSites", "starting download Task");
                SitesDownloadTask download = new SitesDownloadTask();
                download.execute();
            } else {
                mAdapter = new SitesAdapter2(getApplicationContext(), -1, SitesXmlPullParser.getStackSitesFromFile(playlistActivity.this));
                sites2List.setAdapter(mAdapter);
            }
        }
    }

    private SitesAdapter2 mAdapter;
    private ListView sites2List;

    //Helper method to determine if Internet connection is available.
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * AsyncTask that will download the xml file for us and store it locally.
     * After the download is done we'll parse the local file.
     */

    public class SitesDownloadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            //Download the file
            try {
                Downloader.DownloadFromUrl("http://kusf.radioactivity.fm/feeds/last25.xml", openFileOutput("StackSites.xml", Context.MODE_PRIVATE));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //setup our Adapter and set it to the ListView.
            mAdapter = new SitesAdapter2(playlistActivity.this, -1, SitesXmlPullParser.getStackSitesFromFile(playlistActivity.this));
            sites2List.setAdapter(mAdapter);
            Log.i("StackSites", "adapter size = " + mAdapter.getCount());
        }

    }
}
