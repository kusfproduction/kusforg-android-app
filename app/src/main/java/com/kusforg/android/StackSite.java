package com.kusforg.android;


/*
 * Data object that holds all of our information about a StackExchange Site.
 */
public class StackSite {

	private String track;
	private String album;
	private String artist;
	private String about;

	public String getTrack() {
		return track;
	}
	public void setTrack(String track) {
		this.track = track;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	@Override
	public String toString() {
		return "StackSite [track=" + track + ", artist=" + artist + ", album=" + album + ", about="
				+ about + "]";
	}
}
